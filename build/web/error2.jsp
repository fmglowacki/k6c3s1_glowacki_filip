<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html>

<f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>parse data error page</title>
    </head>
    <body>
        <h1>Parse Error</h1>
        <h:form id="form_parser_error">  
            <h3><h:outputText value="Error while parsing data: parameters have to be delimited with semicolons." /></h3><br>
            <h:commandButton value="Arithmetic mean calculator" action="true" />
        </h:form>
    </body>
</html>
</f:view>