/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab3;

import java.beans.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fglow
 */
public class MeanCalculatorBean implements Serializable {
    
    public static final String PROP_SAMPLE_PROPERTY = "sampleProperty";
    
    String parametersString;
    private String[] parameters;
    float result;
    List<Float> oldResults;
    
    private PropertyChangeSupport propertySupport;
    
    public MeanCalculatorBean() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
    public String getParametersString() {
        return parametersString;
    }
    
    public void setParametersString(String value) {
        parametersString = value;
    }

    public float getResult() {
        return result;
    }

    public void setResult(float result) {
        float oldValue = this.result;
        this.result = result;
        propertySupport.firePropertyChange(PROP_SAMPLE_PROPERTY, oldValue, this.result);
    }

    public List<Float> getOldResults() {
        return oldResults;
    }

    public void setOldResults(List<Float> oldResults) {
        this.oldResults = oldResults;
    }
     
    
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
    
    public Boolean calculate() {
        result = 0;
        if(parametersString.matches("[0-9]+(;[0-9]+)*")) {
            parameters = parametersString.split(";");
            int numberOfParameters = 0;
            for(String param : parameters) {
                numberOfParameters++;
                result += Float.parseFloat(param);
            }
            result /= numberOfParameters;
            
            if(oldResults == null) oldResults = new ArrayList<>();
            oldResults.add(this.result);
            
            return true;
        } else {
            return false;
        }  
    } 
}
