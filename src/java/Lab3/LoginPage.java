/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab3;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author fglow
 */
@Named(value = "login")
@RequestScoped
public class LoginPage {

    
    private String username;
    private String password;
    
    public LoginPage() {
    }
    
    public String getUsername(){ 
        return username;
    }
    public void setUsername(String nazwa){
        this.username = nazwa; 
    }
    
    public String getPassword(){
        return password;
    }
    
    public void setPassword(String haslo){
        this.password = haslo;
    }
    
    public Boolean checkCredentials(){
        if (username.equals("admin") && password.equals("admin"))
            return true;
        else
            return false;
    }
}
