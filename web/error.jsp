<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html>

<f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>login error page</title>
    </head>
    <body>
        <h1>Login Error</h1>
        <h:form id="form_login_error">
            <h3><h:outputText value="Error while signing in: wrong username or password." /> </h3><br>
            <h:commandButton value="sign in" action="retry" />
        </h:form> 
    </body>
</html>
</f:view>