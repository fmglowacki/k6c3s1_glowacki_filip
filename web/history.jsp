<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="c"uri= "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>History</h1>
        <jsp:useBean id="dBean" class="Lab3.MeanCalculatorBean" scope="session" />
        <h:form id="form_mean">
            <ol>
            <c:forEach items="${dBean.oldResults}" var="oldResult">
                <li><c:out value="${oldResult}"/></li>
            </c:forEach>
            </ol>
            <h:commandButton value="Arithmetic mean calculator" action="calculator" />
        </h:form>
    </body>
</html>
</f:view>
