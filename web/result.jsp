<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html>

<f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Result Page</title>
    </head>
    <body>
        <h1>Arithmetic Mean Calculator</h1>
        <jsp:useBean id="dBean" class="Lab3.MeanCalculatorBean" scope="session" />
            Obliczona średnia: <jsp:getProperty name="dBean" property="result" /><br>
        <h:form id="form_result">
            <h:commandButton value="Arithmetic mean calculator" action="calculator" />
            <h:commandButton value="History" action="history" />
        </h:form>
    </body>
</html>
</f:view>
