<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html>

<f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
    </head>
    <body>
        <h1>Sign in!</h1>
        <h:form id="form_login">
            <h:outputText value ="Username:" /> <br>
            <h:inputText value="#{login.username}" /><br>
            <h:outputText value="Password:" /> <br>
            <h:inputSecret value="#{login.password}" /> <br>
            <h:commandButton value="Sign in" action="#{login.checkCredentials}" /><br>
            <h:outputText value ="(username: admin, password: admin)" /> <br>
        </h:form>
    </body>
</html>
</f:view>