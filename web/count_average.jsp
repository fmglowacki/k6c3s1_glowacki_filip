<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html>

<f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>arithmetic mean calculator </title>
    </head>
    <body>
        <h1>Arithmetic Mean Calculator</h1>
        <h:form id="form_mean">
            <h:outputText value ="Please enter parameters (delimited with semicolons):" /> <br>
            <h:inputText id="result" value="#{dBean.parametersString}" />
            <jsp:useBean id="dBean" class="Lab3.MeanCalculatorBean" scope="session" />
            <jsp:setProperty name="dBean" property="parametersString" value="${parameters}" /><br>
            <h:commandButton value="Calculate" action="#{dBean.calculate()}" />
            <h:commandButton value="History" action="history" />
        </h:form>
</html>
</f:view>